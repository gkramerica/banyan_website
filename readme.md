python -m venv .venv
source .venv/bin/activate

pip install django==3.2.5
django-admin startproject banyan_service .

python manage.py startapp website
python manage.py migrate  
python manage.py createsuperuser
python manage.py runserver localhost:8000

pip freeze > requirements.txt
