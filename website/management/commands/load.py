from django.core.management.base import BaseCommand, CommandError
from website.load_players import load_players


class Command(BaseCommand):
    help = "Loads old data"

    def handle(self, *args, **options):
        load_players()

        self.stdout.write(self.style.SUCCESS("Successfully loaded"))
