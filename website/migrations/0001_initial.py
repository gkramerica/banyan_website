# Generated by Django 3.2.5 on 2021-07-13 22:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('short_name', models.CharField(max_length=100)),
                ('tee', models.CharField(max_length=10)),
            ],
            options={
                'db_table': 'course',
            },
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('current_tee', models.CharField(default='blue', max_length=10)),
                ('current_handicap', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'player',
            },
        ),
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('year', models.IntegerField()),
                ('player1', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='player1', to='website.player')),
                ('player2', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='player2', to='website.player')),
            ],
            options={
                'db_table': 'team',
            },
        ),
        migrations.CreateModel(
            name='PlayerRound',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('year', models.IntegerField()),
                ('week', models.IntegerField(null=True)),
                ('date_played', models.DateField(null=True)),
                ('handicap', models.IntegerField()),
                ('score', models.IntegerField(null=True)),
                ('net_score', models.IntegerField(null=True)),
                ('points', models.IntegerField(null=True)),
                ('unofficial', models.BooleanField(default=False)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='website.course')),
                ('player', models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, to='website.player')),
            ],
            options={
                'db_table': 'player_round',
            },
        ),
        migrations.CreateModel(
            name='PlayerHole',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('hole', models.IntegerField()),
                ('score', models.IntegerField()),
                ('gross_score', models.IntegerField()),
                ('net_score', models.IntegerField()),
                ('points1', models.IntegerField(null=True)),
                ('round', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='website.playerround')),
            ],
            options={
                'db_table': 'player_hole',
            },
        ),
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('year', models.IntegerField()),
                ('week', models.IntegerField()),
                ('date_played', models.DateField(null=True)),
                ('points1', models.IntegerField(null=True)),
                ('points2', models.IntegerField(null=True)),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='website.course')),
                ('roundA1', models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, related_name='p1A', to='website.playerround')),
                ('roundA2', models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, related_name='p2A', to='website.playerround')),
                ('roundB1', models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, related_name='p1B', to='website.playerround')),
                ('roundB2', models.ForeignKey(null=True, on_delete=django.db.models.deletion.RESTRICT, related_name='p2B', to='website.playerround')),
                ('team1', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='team1', to='website.team')),
                ('team2', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, related_name='team2', to='website.team')),
            ],
            options={
                'db_table': 'match',
            },
        ),
        migrations.CreateModel(
            name='Hole',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('number', models.IntegerField()),
                ('par', models.IntegerField()),
                ('handicap', models.IntegerField()),
                ('course', models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='website.course')),
            ],
            options={
                'db_table': 'hole',
            },
        ),
    ]
