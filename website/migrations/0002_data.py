from django.db import migrations, models
import django.db.models.deletion

HOLE_HANIDCAPS_BLUE = [
    [10, 6, 12, 2, 14, 8, 4, 16, 18],
    [9, 15, 17, 7, 1, 11, 5, 3, 13],
    [4, 6, 2, 8, 12, 16, 18, 10, 14],
    [3, 5, 7, 13, 15, 17, 9, 1, 11],
]

HOLE_HANIDCAPS_RED = [
    [12, 6, 8, 4, 16, 10, 2, 18, 14],
    [13, 5, 9, 7, 3, 15, 11, 1, 17],
    [4, 14, 6, 2, 18, 10, 16, 8, 12],
    [3, 5, 11, 13, 7, 17, 1, 15, 9],
]

HOLE_PAR = [
    [3, 5, 4, 4, 3, 4, 5, 3, 4],
    [4, 4, 4, 4, 3, 5, 4, 5, 3],
    [4, 4, 5, 4, 3, 4, 3, 4, 4],
    [5, 4, 4, 4, 4, 3, 4, 3, 5],
]


def load(apps, schema):
    Course = apps.get_model("website", "Course")
    Hole = apps.get_model("website", "Hole")

    courses = ["Lakeside", "Riverside"]
    short_names = ["LK", "RS"]
    nines = ["Front", "Back"]
    tees = ["blue", "yellow", "red"]

    index = 0
    for c in courses:
        for n in nines:
            for tee in tees:
                short_name = "LK" if c == "Lakeside" else "RS"
                short_name += n[1]
                cs = Course(name=f"{c} {n}", short_name=short_name, tee=tee)
                cs.save()

                for i in range(len(HOLE_PAR)):
                    h = i + 1
                    if n == "Back":
                        h += 0
                    if tee == "blue":
                        hcp = HOLE_HANIDCAPS_BLUE
                    else:
                        hcp = HOLE_HANIDCAPS_RED

                    hole = Hole(
                        number=h, par=HOLE_PAR[index][i], handicap=hcp[index][i]
                    )

            index += 1


def delete(apps, schema):
    Course = apps.get_model("website", "Course")
    Hole = apps.get_model("website", "Hole")

    Hole.objects.all().delete()
    Course.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [("website", "0001_initial")]

    operations = [migrations.RunPython(load, delete)]
