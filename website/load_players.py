import os
from bs4 import BeautifulSoup
from .models import Player, Team


def load_players(year=2021):
    file_name = os.path.join(
        os.path.dirname(__file__), "files", f"handicaps-{year}.html"
    )
    with open(file_name) as f:
        soup = BeautifulSoup(f.read(), "html.parser")
        table = soup.find("table", {"frame": "void"})
        last_player = None

        for row in table.findAll("tr"):
            cells = row.findAll("td")
            data = [ele.text.strip() for ele in cells]
            if len(data) > 4:
                team = data[0]
                names = data[1].split(" ")
                if len(names) == 2:
                    tee = "blue" if data[4] == "" else "yellow"
                    try:
                        handicap = int(data[5]) if data[4] == "" else int(data[4])
                    except:
                        handicap = None

                    if data[15] == "***":
                        handicap = None

                    qs = Player.objects.filter(first_name=names[0], last_name=names[1])
                    if qs.count() == 0:
                        player = Player(
                            first_name=names[0],
                            last_name=names[1],
                            current_tee=tee,
                            current_handicap=handicap,
                        )
                    else:
                        player = qs.first()
                        player.current_tee = tee
                        player.current_handicap = handicap
                    player.save()

                    if len(team) > 0:
                        if last_player is None:
                            last_player = player
                        else:
                            qs = Team.objects.filter(name=team, year=year)
                            if qs.count() == 0:
                                t = Team(
                                    name=team,
                                    player1=last_player,
                                    player2=player,
                                    year=year,
                                )
                            else:
                                t = qs.first()
                                t.player1 = last_player
                                t.player2 = player

                            t.save()
