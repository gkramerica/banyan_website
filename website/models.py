from django.db import models


class Course(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=100)
    tee = models.CharField(max_length=10)

    class Meta:
        db_table = "course"


class Hole(models.Model):
    id = models.BigAutoField(primary_key=True)
    course = models.ForeignKey(Course, on_delete=models.RESTRICT)
    number = models.IntegerField()
    par = models.IntegerField()
    handicap = models.IntegerField()

    class Meta:
        db_table = "hole"


class Player(models.Model):
    id = models.BigAutoField(primary_key=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    current_tee = models.CharField(max_length=10, default="blue")
    current_handicap = models.IntegerField(null=True)

    class Meta:
        db_table = "player"


class Team(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)
    year = models.IntegerField()
    player1 = models.ForeignKey(
        Player, on_delete=models.RESTRICT, related_name="player1"
    )
    player2 = models.ForeignKey(
        Player, on_delete=models.RESTRICT, related_name="player2"
    )

    class Meta:
        db_table = "team"


class PlayerRound(models.Model):
    id = models.BigAutoField(primary_key=True)
    course = models.ForeignKey(Course, on_delete=models.RESTRICT)
    year = models.IntegerField()
    week = models.IntegerField(null=True)
    date_played = models.DateField(null=True)
    player = models.ForeignKey(Player, null=True, on_delete=models.RESTRICT)
    handicap = models.IntegerField()
    score = models.IntegerField(null=True)
    net_score = models.IntegerField(null=True)
    points = models.IntegerField(null=True)
    unofficial = models.BooleanField(default=False)

    class Meta:
        db_table = "player_round"


class PlayerHole(models.Model):
    id = models.BigAutoField(primary_key=True)
    round = models.ForeignKey(PlayerRound, on_delete=models.RESTRICT)
    hole = models.IntegerField()
    score = models.IntegerField()
    gross_score = models.IntegerField()
    net_score = models.IntegerField()
    points1 = models.IntegerField(null=True)

    class Meta:
        db_table = "player_hole"


class Match(models.Model):
    id = models.BigAutoField(primary_key=True)
    year = models.IntegerField()
    week = models.IntegerField()
    course = models.ForeignKey(Course, on_delete=models.RESTRICT)
    date_played = models.DateField(null=True)
    team1 = models.ForeignKey(Team, on_delete=models.RESTRICT, related_name="team1")
    team2 = models.ForeignKey(Team, on_delete=models.RESTRICT, related_name="team2")
    roundA1 = models.ForeignKey(
        PlayerRound, null=True, on_delete=models.RESTRICT, related_name="p1A"
    )
    roundB1 = models.ForeignKey(
        PlayerRound, null=True, on_delete=models.RESTRICT, related_name="p1B"
    )
    roundA2 = models.ForeignKey(
        PlayerRound, null=True, on_delete=models.RESTRICT, related_name="p2A"
    )
    roundB2 = models.ForeignKey(
        PlayerRound, null=True, on_delete=models.RESTRICT, related_name="p2B"
    )
    points1 = models.IntegerField(null=True)
    points2 = models.IntegerField(null=True)

    class Meta:
        db_table = "match"
